import { Readable, PassThrough } from "stream"
import { createHash } from "crypto"
import { platform } from "os"
import { createWriteStream, createReadStream, constants, PathLike } from "fs"
import { access, link, mkdir } from "@improved/node/fs"
import got from "got"
import { spawn, SpawnOptionsWithoutStdio } from "child_process"
import { basename, join, isAbsolute, sep, posix, resolve } from "path"
import { getPath } from "rsync-or-deltacopy"
import { createGunzip } from "zlib"
import * as Url from "url"
import envPaths from "env-paths"
import { directory as tempDirectory } from "tempy"

const pkg = require("./package")

const paths = envPaths(pkg.name)
const mkdirp = (path: PathLike) => mkdir(path, { recursive: true })

export function gunzipThenRsync({
  gunzipUrl,
  rsyncSource,
  cacheFilename,
  rsyncSpawnOpts
}: GunzipThenRsyncOpts): Readable {
  const ret = new PassThrough()
  const errorCb = (err: Error) => ret.emit("error", err)
  ;(async () => {
    try {
      const { name, version } = pkg
      const rsyncBinPath = await getPath()
      if (rsyncBinPath == null) {
        throw new Error("rsync binary not found")
      }
      if (!(gunzipUrl && rsyncSource)) {
        throw new Error("gunzipUrl and rsyncUrl must be defined")
      }
      // Default cache file location to ${OS cache}/${unique hash}
      cacheFilename =
        cacheFilename ||
        paths.cache + "/" + hash({ name, version, gunzipUrl, rsyncSource })

      // If cached file doesn't exist
      // Retrieve gzipped file and stream it to the function output
      // and also cache it
      if (!(await exists(cacheFilename))) {
        await mkdirp(resolve(`${cacheFilename}/..`))
        const fetchStream = got.stream(gunzipUrl)
        const cacheStream = createWriteStream(cacheFilename)
        const gunzip = createGunzip()

        // Attach error handlers
        fetchStream.on("error", errorCb)
        gunzip.on("error", errorCb)

        // Main pipe: fetch -> gunzip -> return stream
        fetchStream.pipe(gunzip).pipe(ret)
        // Secondary pipe: gunzip -> cache file
        gunzip.pipe(cacheStream)
        return
      }

      // If cached file exists
      // rsync the existing file

      const rsyncBasename = basename(rsyncSource)
      const cacheBasename = basename(cacheFilename)

      // Check if same filename
      if (rsyncBasename === cacheBasename) {
        await rsyncFile({
          rsyncBinPath,
          rsyncSource,
          stageDir: join(cacheFilename, ".."),
          opts: rsyncSpawnOpts
        })
      } else {
        // Stage the file to a random directory to have the same name as the
        // remote resource
        const stagingDir = await tempDirectory()
        const stagingFilename = join(stagingDir, rsyncBasename)
        await link(cacheFilename, stagingFilename)

        // rsync
        await rsyncFile({
          rsyncBinPath,
          rsyncSource,
          stageDir: paths.cache,
          opts: rsyncSpawnOpts
        })
      }

      const s = createReadStream(cacheFilename)
      s.on("error", errorCb)
      s.pipe(ret)
    } catch (err) {
      ret.emit("error", err)
    }
  })()
  return ret
}

async function rsyncFile({
  rsyncBinPath,
  rsyncSource,
  stageDir,
  opts
}: {
  rsyncBinPath: string
  rsyncSource: string
  stageDir: string
  opts?: SpawnOptionsWithoutStdio
}) {
  const run = (rsyncSource: string, stageDir: string) =>
    new Promise((r, j) => {
      const args = ["-ztq", rsyncSource, stageDir]
      const cp = spawn(rsyncBinPath, args, opts)
      cp.on("exit", x => {
        if (typeof x === "string" || x !== 0) {
          j(new Error(`rsync exited with code ${x}`))
        }
        r()
      })
      cp.on("error", err => j(err))
    })
  try {
    await run(rsyncSource, stageDir)
  } catch (err) {
    const newRsyncSource = toCygwinPath(rsyncSource)
    const newStageDir = toCygwinPath(stageDir)
    if (!(newRsyncSource === rsyncSource && newStageDir === stageDir)) {
      await run(newRsyncSource, newStageDir)
    } else {
      throw err
    }
  }
}

function hash(x: any): string {
  return createHash("sha256")
    .update(JSON.stringify(x))
    .digest("hex")
}

const protocolsSet: ReadonlySet<string> = new Set([
  "http:",
  "https:",
  "rsync:",
  "ssh:"
])

function toCygwinPath(p: string) {
  const urlParsed = Url.parse(p)
  if (urlParsed.protocol != null && protocolsSet.has(urlParsed.protocol)) {
    return p
  }
  if (isAbsolute(p) && platform() === "win32") {
    const parts = p.split(sep)
    const matchResult = parts[0].match(/^([a-zA-Z]):$/)
    if (matchResult != null) {
      const [, drive] = matchResult
      parts.shift()
      return posix.join("/cygdrive", drive.toLowerCase(), ...parts)
    }
  }
  return p
}

async function exists(path: string) {
  try {
    await access(path, constants.R_OK | constants.W_OK)
    return true
  } catch (err) {
    return false
  }
}

export interface GunzipThenRsyncOpts {
  /**
   * The location of a gzipped version of the resource.
   */
  gunzipUrl: string
  /**
   * The location of the resource that can be fed into `rsync`
   */
  rsyncSource: string
  /**
   * Optional. The name of the file to cache the resource into.
   * If not given, a temporary file will be provided for you.
   */
  cacheFilename?: string
  /**
   * Passed to the `spawn` call for `rsync`.
   */
  rsyncSpawnOpts?: SpawnOptionsWithoutStdio
}

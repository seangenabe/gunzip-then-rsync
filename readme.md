# gunzip-then-rsync

`gunzip` a file from a URL the first time, and `rsync` the cached file subsequently.

## Usage

```typescript
import { gunzipThenRsync } from "gunzip-then-rsync"
```

### gunzipThenRsync(options): Readable

Options:

* gunzipUrl: The location of a gzipped version of the resource.
* rsyncSource: The location of the resource that can be fed into `rsync`
* cacheFilename: Optional. The name of the file to cache the resource into. If not given, a temporary file will be provided for you.
* rsyncOpts: Passed to the `spawn` call for `rsync`.

Returns: A `Readable` readable stream that can be read to fetch an updated copy of the resource.

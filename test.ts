import test from "ava"
import { readFileSync, writeFileSync, mkdtempSync } from "fs"
import { gunzipThenRsync } from "."
import { join } from "path"
import getStream from "get-stream"
import { gzipSync } from "zlib"
import { createServer, Server } from "http"
import { parse as urlParse } from "url"
import getPort from "get-port"
import { tmpdir } from "os"

const pkg = require("./package")
const mkt = () => mkdtempSync(`${tmpdir()}/${pkg.name}-test`)

const getInvalidFile = () => join(mkt(), "file")

const fooGzip = gzipSync(Buffer.from("foo"))

let server: Server
let port: number

test.before(async () => {
  server = createServer((req, res) => {
    if (!req.url) {
      res.writeHead(404).end()
      return
    }
    if (urlParse(req.url).pathname === "/file.gz" && req.method === "GET") {
      res.end(fooGzip)
      return
    }
    res.writeHead(404).end()
  })
  port = await getPort()
  server.listen(port)
})

test.after.cb(t => {
  server.close(t.end)
})

test("cache miss", async t => {
  const dir2 = mkt()
  const file2 = join(dir2, "file")

  t.log(1)
  const stream = gunzipThenRsync({
    gunzipUrl: `http://127.0.0.1:${port}/file.gz`,
    rsyncSource: getInvalidFile(),
    cacheFilename: file2
  })
  t.log(2)

  const contents = await getStream(stream)
  t.log(3)
  t.is(contents, "foo")

  t.is(readFileSync(dir2 + "/file", { encoding: "utf8" }), "foo")
})

test("invalid gunzip URL", async t => {
  await t.throwsAsync(async () => {
    await getStream(
      gunzipThenRsync({
        gunzipUrl: `invalidprotocol://invalid`,
        rsyncSource: getInvalidFile()
      })
    )
  })
})

test("cache hit", async t => {
  const dir1 = mkt()
  const file1 = join(dir1, "file")
  writeFileSync(file1, "foobar")

  const dir2 = mkt()
  const file2 = join(dir2, "file")
  writeFileSync(file2, "foo")

  const stream = gunzipThenRsync({
    gunzipUrl: `http://127.0.0.1:1234/invalidfile.gz`,
    rsyncSource: file1,
    cacheFilename: file2
  })

  const contents = await getStream(stream)
  t.is(contents, "foobar")

  t.is(readFileSync(file2, { encoding: "utf8" }), "foobar")
})
